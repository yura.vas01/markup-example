import WorksService from '../services/WorksService';
import WorkCard from "./work-card";

export default class Works {
  constructor(node) {
    this.container = node;
    this.categoriesGroup = Array.from(this.container.querySelectorAll('.js-work-categories'));
    this.works = this.container.querySelector('.js-works-container');

    this.worksService = new WorksService();
    this.worksData = this.worksService.getWorks();
    this.render();

    this.addCategoriesFilter();
  }

  async addCategoriesFilter() {
    this.categoriesGroup.forEach((categoryGroup) =>
      categoryGroup.addEventListener('click', async (e) => this.onClickCategory(e, categoryGroup)))
  }

  async onClickCategory(e, categoryGroup) {
    const target = e.target.closest('.js-categories-item');

    if (!target) return;

    const filterName = target.dataset.filter;

    Array.from(categoryGroup.querySelectorAll('.js-categories-item'))
      .forEach((category) => category.classList.remove('works-categories__item_active'));

    target.classList.add('works-categories__item_active');

    this.worksData = this.worksService.getWorks();

    filterName !== 'all'
      ? this.worksData = this.worksData.filter(
      (workData) => workData.category.name === filterName) : '';

    this.render();
  }

  render() {
    this.works.innerHTML = `
      ${ this.worksData.map((work) => `
          <div class="work-card js-work-card">
            <img src="${work.img}" alt="">            
            <div class="work-card__overlay js-work-card-overlay">
              <h4 class="title is-3 is-white work-card__title">
                ${work.title}
              </h4>
              <span class="text is-3 is-white work-card__description">
                ${work.description}
              </span>
              <div class="work-card__awards js-work-card-awards">                
                
                ${work.awards.map((award) => `                
                  <div class="text is-4 is-white work-card__award js-work-card-award">
                    ${award.position}
                    <div class="award-overlay js-award-overlay">
                      <span class="text is-4 is-white award-overlay__title">
                         ${award.title}
                      </span>
                      <span class="text is-3 is-white award-overlay__description">
                         ${award.description}
                      </span>
                    </div>
                  </div>
                `).join('')} 
                        
                </div>
            </div>
          </div>
      `).join('')}`;

    Array.from(document.querySelectorAll('.js-work-card'))
      .forEach((workCard) => new WorkCard(workCard));
  }
}
