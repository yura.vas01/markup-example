export default class WorkCard {
  constructor(node) {
    this.container = node;
    this.awardsWrapper = this.container.querySelector('.js-work-card-awards');
    this.awards = Array.from(this.container.querySelectorAll('.js-work-card-award'));

    this.init();
  }

  init() {
    this.awards.forEach((award) => {
      award.addEventListener('click', (e) => {

        const target = e.target;
        const children = Array.from(target.closest('.js-work-card-overlay').children);

        if (!target.classList.contains('js-work-card-award')) return;

        this.awards.forEach((innerAward) => {
          if (innerAward !== award) {
            innerAward.classList.remove('active');
            innerAward.classList.add('is-gray');
          } else {
            innerAward.classList.remove('is-gray');
          }
        });

        target.classList.toggle('active');

        if (target.classList.contains('active')) {
          children.forEach((child) =>
            child !== this.awardsWrapper ? child.classList.add('blured') : '');
        } else {
          children.forEach((child) =>
            child !== this.awardsWrapper ? child.classList.remove('blured') : '');

          this.awards.forEach((innerAward) => innerAward.classList.remove('is-gray'));
        }
      })
    });
  }
}
