export default class Header {
  constructor(node) {
    this.container = node;
    this.mobileMenu = this.container.querySelector('.js-mobile-menu');
    this.mobileMenuTrigger = this.mobileMenu.querySelector('.js-mobile-menu-trigger');
    this.init();
    this.addMenuListener();
    this.onScrollWindow();
  }

  init() {
    if (!document.querySelector('.main-page')) {
      this.container.querySelector('.js-header-label').style.display = 'none';
    }

    window.addEventListener('scroll', () => {
      if (window.pageYOffset > 20) {
        this.container.classList.add('scrolled');
      } else {
        this.container.classList.remove('scrolled');
      }

      if (window.pageYOffset > this.container.clientHeight - 50) {
        this.mobileMenu.classList.add('visible')
      } else {
        this.mobileMenu.classList.remove('visible')
      }
    });
  }

  onScrollWindow() {

  }

  addMenuListener() {
    this.mobileMenuTrigger.addEventListener('click', () => {
      this.mobileMenu.classList.toggle('active');
    });
  };

}
