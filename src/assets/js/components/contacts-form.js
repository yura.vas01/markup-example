import IMask from 'imask';

export default class ContactsForm {
  constructor(node) {
    this.container = node;
    this.fileApi = !!( window.File && window.FileReader && window.FileList && window.Blob );
    this.filePickerInput = this.container.querySelector('.js-file-picker');
    this.filePickerBtn = this.container.querySelector('.js-file-picker-btn');
    this.filePickerClose = this.container.querySelector('.js-file-picker-close');
    this.filePickerWrapper = this.container.querySelector('.js-file-picker-wrap');
    this.filePickerProgress = this.container.querySelector('.js-file-picker-progress');
    this.filePickerProgressCount = this.container.querySelector('.js-file-picker-progress-count');
    this.phoneMask = { mask: '{8}0000000000'};
    this.nameMask = { mask: /^([a-zа-яё\s]+)$/i };
    this.onChangePicker();
    this.onRemoveFile();
    this.setValidation();
    this.setMasks();
  }

  setMasks() {
    Array.from(this.container.querySelectorAll('.js-mask-phone'))
      .forEach((phone) => new IMask(phone, this.phoneMask));

    Array.from(this.container.querySelectorAll('.js-mask-name'))
      .forEach((name) => new IMask(name, this.nameMask));
  }

  setValidation() {
    new window.JustValidate('.js-contacts-form', {
      rules: {
        name: {
          required: true
        },
        tel: {
          required: true,
          minLength: 11
        },
        checkbox: {
          required: true
        }
      },
      messages: {
        name: {
          required: 'Вы не указали имя'
        },
        tel: {
          required: 'Вы не указали телефон',
          minLength: 'Вы указали неверный телефон'
        },
        email: {
          email: 'Вы указали неверный email',
          required: 'Вы не указали email'
        },
        checkbox: {
          required: 'Вы не согласились с правилами'
        }
      },

      submitHandler: (form, values, ajax) => {
        console.log(form, values);
        console.log(this.filePickerInput.files[0])
      }

    });

  }

  onChangePicker() {
    this.filePickerInput.addEventListener('change', () => {
      let fileName = 'Загрузить';
      const file = this.filePickerInput.files[0];

      if ( this.fileApi && file ) {
        fileName = file.name;
      }

      if ( !file )
        return;

      if (file.size > 32000000) {

        this.onLoadedOverSize();

      } else {

        this.onSuccessLoad(fileName);

      }
    });
  }

  onLoadedOverSize() {
    Array.from(this.filePickerWrapper.querySelectorAll('.base-file-picker__error'))
      .forEach((err) => err.remove());

    const errorEl = document.createElement('span');
    errorEl.classList.add('base-file-picker__error');
    errorEl.innerHTML = 'Загрузите файл меньше 32MB';

    this.filePickerWrapper.appendChild(errorEl);
    this.filePickerBtn.textContent = 'Загрузить';
    this.filePickerBtn.title = 'Загрузить';
    this.filePickerInput.value = '';

    this.filePickerBtn.parentElement.querySelector('.js-file-picker-size').style.display = 'block';
  }

  onSuccessLoad(fileName) {
    this.startProgress(10000);

    Array.from(this.filePickerWrapper.querySelectorAll('.base-file-picker__error'))
      .forEach((err) => err.remove());

    this.filePickerBtn.textContent = fileName;
    this.filePickerBtn.title = fileName;

    this.filePickerBtn.classList.add('active');
    this.filePickerClose.classList.add('active');
    this.filePickerBtn.parentElement.querySelector('.js-file-picker-size').style.display = 'none';
  }

  startProgress(size) {
    let i = 0;

    const timer = setInterval(() => {
      i++;
      this.filePickerProgress.style.width = `${i}%`;
      this.filePickerProgressCount.textContent = `${i}%`;
      if (i === 100) {
        clearInterval(timer);
        this.filePickerProgress.style.width = `0%`;
        this.filePickerProgressCount.textContent = `Загружен`;
      }
    }, 10)

  }

  onRemoveFile() {
    this.filePickerClose.addEventListener('click', (e) => {
      e.preventDefault();

      this.filePickerInput.value = '';
      this.filePickerBtn.classList.remove('active');
      this.filePickerClose.classList.remove('active');
      this.filePickerBtn.textContent = 'Загрузить';
      this.filePickerBtn.title = 'Загрузить';
      this.filePickerProgress.style.width = '0%';
      this.filePickerProgressCount.textContent = ``;
      this.filePickerBtn.parentElement.querySelector('.js-file-picker-size').style.display = 'block';
    })
  }
}
