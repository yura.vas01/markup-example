import '@babel/polyfill';
import 'just-validate/dist/js/just-validate.min';
import { fetch } from 'whatwg-fetch';


import './polyfills/closest'

import Header         from './components/header';
import ContactsForm   from './components/contacts-form';
import WorkCard       from './components/work-card';
import Works          from './components/works';

Array.from(document.querySelectorAll('.js-header'))
  .forEach((header) => new Header(header));

Array.from(document.querySelectorAll('.js-contacts-form'))
  .forEach((form) => new ContactsForm(form));

Array.from(document.querySelectorAll('.js-work-card'))
  .forEach((workCard) => new WorkCard(workCard));

Array.from(document.querySelectorAll('.js-works-page'))
  .forEach((works) => new Works(works));

if (document.body.querySelector('.js-main-page')) {
  document.body.classList.add('main-page-body')
}
