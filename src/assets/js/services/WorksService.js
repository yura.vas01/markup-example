export default class WorksService {

  getWorks() {
    return [
      {
        category: {
          label: "Ритейл",
          name: "retail"
        },
        img: "./assets/img/item.png",
        title: "Магазин",
        description: "Ecom приложение для сети магазинов",
        awards: [
          {
            position: "1",
            title: "Премия",
            description: "1 место за Мобильное приложение"
          },
          {
            position: "2",
            title: "Премия",
            description: "2 место за Мобильное приложение"
          }
        ]
      },
      {
        category: {
          label: "Ритейл",
          name: "retail"
        },
        img: "./assets/img/item.png",
        title: "Магазин",
        description: "Ecom приложение для сети магазинов",
        awards: [
          {
            position: "1",
            title: "Премия",
            description: "1 место за Мобильное приложение"
          },
          {
            position: "2",
            title: "Премия",
            description: "2 место за Мобильное приложение"
          }
        ]
      },
      {
        category: {
          label: "Медия",
          name: "media"
        },
        img: "./assets/img/item.png",
        title: "Магазин",
        description: "Ecom приложение для сети магазинов",
        awards: [
          {
            position: "1",
            title: "Премия",
            description: "1 место за Мобильное приложение"
          },
          {
            position: "2",
            title: "Премия",
            description: "2 место за Мобильное приложение"
          }
        ]
      },
      {
        category: {
          label: "Медия",
          name: "media"
        },
        img: "./assets/img/item3.png",
        title: "Магазин",
        description: "Ecom приложение для сети",
        awards: []
      },
      {
        category: {
          label: "Ритейл",
          name: "retail"
        },
        img: "./assets/img/item.png",
        title: "Магазин",
        description: "Ecom приложение для сети магазинов",
        awards: [
          {
            position: "1",
            title: "Премия",
            description: "1 место за Мобильное приложение"
          },
          {
            position: "2",
            title: "Премия",
            description: "2 место за Мобильное приложение"
          }
        ]
      },
      {
        category: {
          label: "Медия",
          name: "media"
        },
        img: "./assets/img/item.png",
        title: "Магазин",
        description: "Ecom приложение для сети магазинов",
        awards: [
          {
            position: "1",
            title: "Премия",
            description: "1 место за Мобильное приложение"
          },
          {
            position: "2",
            title: "Премия",
            description: "2 место за Мобильное приложение"
          }
        ]
      },
      {
        category: {
          label: "Медия",
          name: "media"
        },
        img: "./assets/img/item.png",
        title: "Магазин",
        description: "Ecom приложение для сети",
        awards: []
      },
      {
        category: {
          label: "Медия",
          name: "media"
        },
        img: "./assets/img/item.png",
        title: "Магазин",
        description: "Ecom приложение для сети магазинов",
        awards: [
          {
            position: "1",
            title: "Премия",
            description: "1 место за Мобильное приложение"
          },
          {
            position: "2",
            title: "Премия",
            description: "2 место за Мобильное приложение"
          }
        ]
      },
      {
        category: {
          label: "Медия",
          name: "media"
        },
        img: "./assets/img/item.png",
        title: "Магазин",
        description: "Ecom приложение для сети",
        awards: []
      }
    ]
  }

}
